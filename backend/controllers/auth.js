var express = require('express')
var router = express.Router();
var UserModel = require('./../models/user.model')
var mapRequest = require('./../common/map_user_req')
var jwt = require('jsonwebtoken')
var config = require('./../config')
// var passwordHash = require('password-hash')
var bcrypt = require('bcryptjs')

function createToken(data) {
    var token = jwt.sign({
        username: data.username,
        id: data._id,
        role: data.role
    }, config.jwtSecret,{
        expiresIn: '10m'
    })
    return token;
}

router.get('/', function (req, res, next) {
    res.render('login')
})

router.get('/register', function (req, res, next) {
    res.render('register')
})

router.post('/', function (req, res, next) {
    console.log('request is here at post request', req.body)

    UserModel.findOne({
        username: req.body.username,
        // password: req.body.password
    }, function (err, user) {
        if (err) {
            return next(err)
        }
        if (user) {
            var hash = user.password
            // console.log('hash is',hash)
            bcrypt.compare(req.body.password, hash, function (err, result) {
                if (result) {
                    // Passwords match
                    var token = createToken(user)
                    res.status(200).json({
                        user: user,
                        token: token
                    })
                } else {
                    // Passwords don't match
                    next({
                        message: 'password doesn\'t match',
                        status: 400
                    })
                }
            });

            // var matched = passwordHash.verify(req.body.password, user.password)
            // if (matched) {
            //     var token = createToken(user)
            //     res.status(200).json({
            //         user: user,
            //         token: token
            //     })
            // } else {
            //     next({
            //         message: 'password doesn\'t match',
            //         status: 400
            //     })
            // }
        } else {
            next({
                message: 'user not found',
                status: 404
            })
        }
    })
})


router.post('/register', function (req, res, next) {
    // res.render('register')
    console.log('post your data here', req.body)
    // data is here now proceed with db
    var newUser = new UserModel({});
    var mappedUser = mapRequest(newUser, req.body)
    // mappedUser.password = passwordHash.generate(req.body.password)

    bcrypt.hash(req.body.password, 10, function (err, hash) {
        mappedUser.password = hash;
        mappedUser.save(function (err, done) {
            if (err) {
                return next(err)
            }
            res.status(200).json(done)
        })
    });

    // mappedUser.save(function (err, done) {
    //     if (err) {
    //         return next(err)
    //     }
    //     res.status(200).json(done)
    // })
})


module.exports = router;