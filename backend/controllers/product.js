var express = require('express')
var router = express.Router()
var productModel = require('./../models/product.model')
var mapProductRequest = require('./../common/product_user_req')
var multer = require('multer')
var fs = require('fs')


// var upload = multer({
//     dest: './files/images'
// })

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

var upload = multer({
    storage: storage
})

module.exports = function () {

    router.route('/')
        // .get(function (req, res, next) {
        //     var condition = {};
        //     if (req.loggedInUser.role !== 1) {
        //         condition.user = req.loggedInUser._id
        //     }
        //     productModel.find(condition)
        //         .sort({
        //             _id: -1
        //         })
        //         .exec(function (err, done) {
        //             if (err) {
        //                 return next(err)
        //             }
        //             res.status(200).json(done)
        //         })
        // })

        .get(function (req, res, next) {
            var condition = {};
            if (req.loggedInUser.role == 1) {
                productModel.find({})
                    .sort({
                        _id: -1
                    })
                    .exec(function (err, product) {
                        if (err) {
                            return next(err)
                        }
                        res.status(200).json(product)
                    })
            } else {
                condition.user = req.loggedInUser._id

                productModel.find(condition)
                    .sort({
                        _id: -1
                    })
                    .exec(function (err, product) {
                        if (err) {
                            return next(err)
                        }
                        res.status(200).json(product)
                    })
            }
        })

        .post(upload.single('image'), function (req, res, next) {
            console.log('your request data is here', req.body)
            console.log('your request fileData is here', req.file)

            if (req.file) {
                var image = req.file.mimetype.split('/')[0]
                if (image !== 'image') {
                    fs.unlink('./files/images/' + req.file.filename, function (err, done) {
                        if (err) {
                            return next(err)
                        }
                        next({
                            message: 'invalid file format',
                            status: 400
                        })
                    });
                return;

                }
            }

            var newProduct = new productModel({})
            var mappedProduct = mapProductRequest(newProduct, req.body)
            mappedProduct.user = req.loggedInUser._id;
            if (req.file) {
                mappedProduct.image = req.file.filename;
            }

            mappedProduct.save(function (err, success) {
                if (err) {
                    return next(err)
                }
                res.status(200).json(success)
            })
        })

    router.route('/search')
        .get(function (req, res, next) {
            var condition = {};
            var searchQuery = mapProductRequest(condition, req.query)
            productModel.find(searchQuery)
                .exec(function (err, products) {
                    if (err) {
                        return next(err)
                    }
                    res.status(200).json(products)
                })
        })

        .post(function (req, res, next) {
            var cond = {};
            var searchCondition = mapProductRequest(cond, req.body)
            productModel.find(searchCondition)
                .exec(function (err, products) {
                    if (err) {
                        return next(err)
                    }
                    res.status(200).json(products)
                })
        })

    router.route('/:id')
        .get(function (req, res, next) {
            productModel.findById(req.params.id)
                .exec(function (err, product) {
                    if (err) {
                        return next(err)
                    }
                    if (product) {
                        res.status(200).json(product)
                    } else {
                        next({
                            message: 'product not found',
                            status: 404
                        })
                    }
                })
        })

        .put(upload.single('image'), function (req, res, next) {
            productModel.findOne({
                    _id: req.params.id
                })
                .exec(function (err, product) {
                    if (err) {
                        return next(err)
                    }
                    if (product) {
                        var oldImage = product.image;
                        var mappedProduct = mapProductRequest(product, req.body)

                        if (req.file) {
                            mappedProduct.image = req.file.filename
                        }

                        mappedProduct.save(function (err, updated) {
                            if (err) {
                                return next(err)
                            }
                            if (req.file) {
                                fs.unlink('./files/images/' + oldImage, function (err, done) {
                                    if (err) {
                                        return next(err)
                                    }
                                    res.status(200).json(done)
                                })
                            }
                            res.status(200).json(updated)
                        })
                    } else {
                        next({
                            message: 'product not found',
                            status: 404
                        })
                    }
                })
        })

        .delete(function (req, res, next) {
            productModel.findById(req.params.id)
                .exec(function (err, product) {
                    if (err) {
                        return next(err)
                    }
                    if (product) {
                        product.remove(function (err, deleted) {
                            if (err) {
                                return next(err)
                            }
                            if (product.image) {
                                fs.unlink('./files/images/' + product.image, function (err, done) {
                                    if (err) {
                                        return next(err)
                                    }
                                    res.status(200).json(done)
                                })
                            }
                            // res.status(200).json(deleted)
                            res.status(200).json({
                                message: 'product deleted'
                            })
                        })
                    } else {
                        next({
                            message: 'product not found',
                            status: 404
                        })
                    }
                })
        })


    return router;
};