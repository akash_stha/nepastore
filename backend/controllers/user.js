var express = require('express')
var router = express.Router()
var UserModel = require('./../models/user.model')
var mapRequest = require('./../common/map_user_req')
var authorize = require('./../middlewares/authorize')

router.get('/', function (req, res, next) {
    UserModel.find({})
        .sort({
            _id: -1
        })
        .exec(function (err, user) {
            if (err) {
                return next(err)
            }
            res.status(200).json(user)
        })
})

router.get('/:id', function (req, res, next) {
    console.log('logged in user', req.loggedInUser)

    var id = req.params.id
    UserModel.findById(id, function (err, user) {
        if (err) {
            return next(err)
        }
        if (user) {
            res.status(200).json(user)
        } else {
            next({
                message: 'user not found',
                status: 404
            })
        }
    })
})

router.put('/:id',authorize, function (req, res, next) {
    var id = req.params.id;
    UserModel.findById(id)
        .exec(function (err, user) {
            if (err) {
                return next(err)
            }
            if (user) {
                var mappedUser = mapRequest(user, req.body)

                mappedUser.save(function (err, done) {
                    if (err) {
                        return next(err)
                    }
                    res.status(200).json(done)
                })
            } else {
                next({
                    message: 'user not found',
                    status: 404
                })
            }
        })
})

router.delete('/:id', function (req, res, next) {
    var id = req.params.id
    UserModel.findById(id)
        .exec(function (err, user) {
            if (err) {
                return next(err)
            }
            if (user) {
                user.remove(function (err, removed) {
                    if (err) {
                        return next(err)
                    }
                    res.status(200).json(removed)
                })
            } else {
                next({
                    message: 'user not found',
                    status: 404
                })
            }
        })
})

router.get('/', function (req, res, next) {
    console.log('req.params', req.query)
    res.json(req.query)
})

module.exports = router;