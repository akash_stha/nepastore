var mongoose = require('mongoose')
var config = require('./config')
var localUrl = config.dbUrl + '/' + config.dbName;
var remoteUrl = 'mongodb+srv://aakashtha:newar@cluster0-wciqh.mongodb.net/test?retryWrites=true&w=majority'

var url;

if (process.env.db == 'remote') {
    url = remoteUrl;
} else {
    url = localUrl;
}

mongoose.connect(url, function (err, done) {
    if (err) {
        console.log('error in connecting db',err)
    } else {
        console.log('database connection is open')
    }
});


// npm run remote (command)