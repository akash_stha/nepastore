var express = require('express')
var app = express()
var morgan = require('morgan')
var cors = require('cors')
var path = require('path')
var config = require('./config')
require('./db')
// importing routes
var authRoute = require('./controllers/auth')
var userRoute = require('./controllers/user')
var productRoute = require('./controllers/product')()
// importing middlewares
var authenticate = require('./middlewares/authenticate')

// middlewares
app.use(morgan('dev'))
app.use(cors());

app.set('view engine', 'pug')
app.set('views', 'views')

app.use('/img', express.static(path.join(__dirname, 'files')))
app.use(express.urlencoded({
    extended: true
}))

app.use(express.json());

app.use('/auth', authRoute)
app.use('/user', authenticate, userRoute)
app.use('/product', authenticate, productRoute)

app.use(function (req, res, next) {
    console.log('i am a 404 handler middleware')
    next({
        message: 'PAGE NOT FOUND',
        status: 404
    })
})

// error handling middleware
app.use(function (err, req, res, next) {
    console.log('i am a error handling middleware', err)
    res.status(err.status || 400);
    res.json({
        message: err.message || err,
        status: err.status || 400
    })
})

// server listenig 
app.listen(config.port, function (err, done) {
    if (err) {
        console.log('server listening failed', err)
    } else {
        console.log('server listening at port', config.port)
        console.log('press ctrl+c to exit')
    }
})