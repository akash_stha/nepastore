import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const authRoute: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent
  },
  {
    path: 'register',
    component: ResetPasswordComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(authRoute)
  ],
  exports: [
    RouterModule
  ],
  providers: [],
})

export class AuthRoutingModule {

}
