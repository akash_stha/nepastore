import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { MsgService } from 'src/app/shared/services/msgService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  submitting = false;
  user;

  constructor(
    public router: Router,
    public authService: AuthService,
    public msgService: MsgService
  ) {
    this.user = new User({});
  }

  ngOnInit() {
    // console.log(this.authService.toUpper('ram is my name'));
  }

  login() {
    this.submitting = true;
    this.authService.login(this.user).subscribe(
      (data: any) => {
        this.msgService.showSuccess('welcome ' + data.user.name);
        this.router.navigate(['/user/']);
        console.log('data from authservice', data);
        localStorage.setItem('token', data.token);
        localStorage.setItem('user', JSON.stringify(data.user));
      },
      error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    );
  }

}
