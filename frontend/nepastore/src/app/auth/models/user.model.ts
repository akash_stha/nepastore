export class User {
  name: string;
  username: string;
  email: string;
  password: string;
  phoneNumber: number;
  temporaryAddress: string;
  gender: string;

  constructor(options: any) {
    this.name = options.name || '' ;
    this.username = options.username || '' ;
    this.password = options.password || '' ;
    this.email = options.email || '' ;
    this.temporaryAddress = options.temporaryAddress || '' ;
    this.phoneNumber = options.phoneNumber || '' ;
    this.gender = options.gender || '' ;
  }

}
