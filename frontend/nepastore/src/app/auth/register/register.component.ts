import { Component, OnInit, NgModule } from '@angular/core';
import {User} from './../models/user.model';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msgService';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  submitting = false;
  user;

  constructor(
    public router: Router,
    public authService: AuthService,
    public msgService: MsgService,
  ) {
    this.user = new User({}) ;
  }

  ngOnInit() {
    console.log('this.user is now ', this.user);
  }

  register() {
    this.submitting = true;
    this.authService.register(this.user)
      .subscribe(
        data => {
          this.msgService.showSuccess('Registration Successfull');
          this.router.navigate(['/auth/login']);
        },
        error => {
          this.msgService.showError(error);
        }
      );
  }

}


