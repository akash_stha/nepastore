import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { environment } from './../../../environments/environment';


@Injectable()

export class AuthService {
  url: string;

  constructor(
    public http: HttpClient
  ) {
    this.url = environment.baseUrl;
  }

  getOptions() {
    // tslint:disable-next-line: prefer-const
    let option = {
      headers: new HttpHeaders({
       'Content-Type': 'application/json'
      })
    };
     return option;
  }

  // getUser() {
  //   return JSON.parse(localStorage.getItem('user'));
  // }

  isLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  isAdmin() {
   const check = JSON.parse(localStorage.getItem('user'));
  //  console.log('check is ', check.role);
   if (check.role === '1') {
     return true;
   } else {
     return false;
   }
  }

  login(data: User) {
    return this.http.post(this.url + 'auth', data, this.getOptions());
  }

  register(data: User) {
    return this.http.post(this.url + 'auth/register', data, this.getOptions());
  }

}
