import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListProductComponent } from './list-product/list-product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { SearchProductComponent } from './search-product/search-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';

import { SharedModule } from './../shared/shared.module';
import { ProductRoutingModule } from './product.routing';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListProductComponent, AddProductComponent, SearchProductComponent, EditProductComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    FormsModule
  ]
})
export class ProductModule { }
