import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class MsgService {

  constructor(
    public toastr: ToastrService
  ) {}

  showSuccess(msg: string) {
    this.toastr.success(msg);
  }
  showInfo(msg: string) {
    this.toastr.info(msg);
  }
  showWarning(msg: string) {
    this.toastr.warning(msg);
  }
  showError(val: any) {
    // tslint:disable-next-line: no-debugger
    // debugger ;
    // check what comes in the value and show appropriate message.
    // this will act as FE error handling middleware.
    // tslint:disable-next-line: prefer-const
    let error = val.error;
    if (error) {
      this.show(error.message);
    }
  }
  show(msg: string) {
    this.toastr.error(msg);
  }

}

