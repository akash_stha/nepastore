import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

// MDB Angular Pro
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { MsgService } from './services/msgService';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    FooterComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    RouterModule
  ],
  exports: [
    PageNotFoundComponent,
    FooterComponent,
    ToolbarComponent
  ],
  providers: [
    MsgService
  ]
})
export class SharedModule { }
