import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  // loggedInUser = false;

  constructor(
    public authService: AuthService,
    public router: Router
  ) {
    //  this.loggedInUser = this.authService.isLoggedIn();
  }

  ngOnInit() {
  }

  checkLogin() {
    return this.authService.isLoggedIn();
  }

  checkAdmin() {
    return this.authService.isAdmin();
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }

}
