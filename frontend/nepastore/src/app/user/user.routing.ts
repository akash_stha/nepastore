import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const userRoute: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'changepassword',
    component: ChangePasswordComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(userRoute)
  ],
  exports: [
    RouterModule
  ],
  providers: [],
})

export class UserRoutingModule {

}
